all: clean tex

clean:
	rm -f {*,capitulos/*}.{aux,bbl,blg,idx,lof,log,lot,pdf,toc,brf,ilg,ind}

tex:
	xelatex projeto.tex
	bibtex projeto.aux
	makeindex projeto.idx
	#makeindex projeto.nlo -s nomencl.ist -o projeto.nls # se usar glossario
	xelatex projeto.tex
	xelatex projeto.tex
